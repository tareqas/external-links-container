// "External Links Container" by Tareq Ahamed
'use strict';

let config = {};
let openTabs = [];
const CONTAINER = 'sandbox';


browser.tabs.onRemoved.addListener(function (tabId, removeInfo) {
    delete openTabs[tabId];
});

browser.webRequest.onBeforeSendHeaders.addListener(function (details) {
    console.log("Loading: " + details.url);
    if (// a set up for opening an external url into separate container
        // config.container !== false && // enable/disable option by user
        !openTabs.includes(details.tabId) && // false -> newtab | true -> sametab
        details.originUrl !== undefined && // defined -> request made from a website
        new URL(details.originUrl).protocol !== 'moz-extension:' && // required otherwise create problem opening container link
        !isSameDomain(details.originUrl, details.url)
    ) {
        openInContainer(details.tabId, details.url, CONTAINER)
        .then(tab => {
            openTabs.push(tab.id);
            browser.tabs.remove(details.tabId);
        });
        return { cancel: true };
    }
    else {
        openTabs.push(details.tabId);
        return { requestHeaders: details.requestHeaders };
    }
}, { urls: ["<all_urls>"], types: ["main_frame"] }, ["blocking", "requestHeaders"]);

//------------- helper functions -------------//
function isSameDomain(url1, url2) { 
    const TLD = ['com', 'info', 'io', 'net', 'org']; // most used TLD for deep matching
    url1 = (url1) ? new URL(url1).hostname.toLowerCase().replace('www.', '') : '';
    url2 = (url2) ? new URL(url2).hostname.toLowerCase().replace('www.', '') : '';
    let urlArr1 = url1.split('.').reverse();
    let urlArr2 = url2.split('.').reverse();
    
    if ((url1 === url2) || (TLD.includes(urlArr1[0]) && TLD.includes(urlArr2[0]) && urlArr1[1] === urlArr2[1])) {
        return true;
    } else {
        return false
    }

}

function openInContainer(sourceTab, url, containerName) {
    return new Promise(async function (resolve) {
        containerName = containerName || 'sandbox';
        browser.contextualIdentities.query({})
        .then(async (identities) => {
            let cookieStoreId;
            for (let identity of identities) {
                if (identity.name === containerName) {
                    cookieStoreId = identity.cookieStoreId;
                    break;
                }
            }
            if (!cookieStoreId) {
                let createContext = await browser.contextualIdentities.create({
                    name: containerName,
                    color: "yellow",
                    icon: "circle"
                });
                cookieStoreId = createContext.cookieStoreId;
            }
            let tab = await browser.tabs.create({
                url: url,
                openerTabId: sourceTab,
                cookieStoreId: cookieStoreId
            });
            resolve(tab);
        })
    })

}

console.log('# External Links Container: background.js is running...');
